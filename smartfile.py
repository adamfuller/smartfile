# Program: smartfile.py
# Author: Adam Fuller
# Date: 30/3/2015
# Description: Provide a file-like object with the following methods:
# read
# write
# seek
# close
#
# Instantiating the smartfile class acts like the Python built-in 'open'.

class smartfile():

	def __init__(self, name, mode='r'):

		import os
		import git

		print "Instantiating smartfile object."

		self.name = name

		print "Name = "+name

		try:
			"""
			We start by assuming that the file is (if it already exists) or will
			be (if it does not yet exist) visible to a git repository.
			"""

			parent_of_name = os.path.split(os.path.abspath(name))[0]

			self.repo = git.Repo(parent_of_name)

			print "Existing repository found."

		except git.InvalidGitRepositoryError:
			
			"""
			If an exception is raised saying that a Repo couldn't be found,
			initialize one in the parent of "name".
			"""

			self.repo = git.Repo.init(parent_of_name)

			print "No repository found.  New repository initialized."
			

		# The working directory of that repository may be name's parent or it
		# may be further up.
		self.repodir = self.repo.working_dir

		print "The repo as far as "+name+" is concerned is at "+self.repodir


		"""
		We need to check if name already exists.
		"""

		if os.path.exists(name):

			"""
			If it does already exist, then we need to check its status in terms
			of version control.
			"""

			print name+" exists."

			if name in self.repo.untracked_files:

				"""
				If 'name' is untracked, then stash /everything/ including
				untracked files.  Checkout 'name' from the stash (checking it
				out seems to put it in the index, i.e. there is no need to
				git-add) and commit changes to 'name' on its own.  Finally, pop
				the stash, which brings us back to the original state, but with
				the changes to 'name' committed.
				"""

				print name+" currently untracked by git."

				self.commit("Starting to track.")

				print "Working directory stashed, "+name+" checked out, committed, and stash popped."

			elif self.dirty:

				"""
				Similarly, if the file is not clean, stage the file (git-add)
				then commit then open the file as requested.
				"""

				print name+" has uncommitted changes."

				self.commit("Existing changes committed.")

				print "Working directory stashed, "+name+" checked out, committed, and stash popped."

			else:

				"""
				Otherwise, the file is present and matches the head commit of
				this branch.  No action required.
				"""

				print name+" is clean."

			"""
			The working copy of the file should now be clean so go ahead and
			open the file as requested.
			"""

			self.f = open(name, mode=mode)

		else:

			"""
			If 'name' does not already exist, then we simply try to open the
			file with the requested mode.
			"""

			self.f = open(name, mode=mode)
	
	def _fdirty(self, other=None):

		"""
		Ask git: are there any differences in this file between the current
		working copy (for other=None) or something else (e.g.
		other=repo.index for the current index), and HEAD?  
		"""

		# Take the diff of HEAD to the current working tree, limited to
		# 'name' only.  Return True if the result of that is an empty list,
		# meaning that there are no differences, otherwise return False.
		try:

			return bool(self.repo.tree(self.repo.head).diff(other=other, paths=self.name)
				or (self.name in self.repo.untracked_files))

		except:
			
			return True

	dirty = property(_fdirty)

	def commit(self, message='Manual smartfile commit.'):
		"""
		Commit only changes to one file amongst potentially other changes.
		"""

		try:

			"""
			Try stashing all changes.  Assuming we're in a repo with some
			commits, this should work.  Stashing may fail if e.g. no commits to
			the repository have been made.
			"""

			print "Trying stash, checkout, add, commit, pop approach."

			self.repo.git.stash()

			"""
			Next, checkout (the file of interest) from the stash.
			"""
			self.repo.git.checkout(['--', self.name])

			# Second, stage the file.
			self.repo.index.add([self.name])

			"""
			Then, commit (that change on its own).
			"""
			self.repo.index.commit(message)

			"""
			Finally, pop the stash.
			"""
			self.repo.git.stash('pop')

		except:

			"""
			If that fails we assume it's because we're in a virgin repo, which
			will fail at the first "stash" command above and therefore the "try"
			block above will have had no side effects.  Proceed to a. check if
			the any uncommitted changes are staged and go ahead and commit them
			first then b. add the file of interest and commit on its own.
			"""

			print "That failed."

			if self.repo.index.diff():
				
				print "Staged changes present."

				self.repo.index.commit('Committing already-staged changes.')

			else:

				print "No staged changes present."

				self.repo.index.add([self.name])

				self.repo.index.commit(message)

	def _fclosed(self):

		return self.f.closed

	closed = property(_fclosed)

	def _fmode(self):

		return self.f.mode

	mode = property(_fmode)

	def close(self, commit=True, message='Changes committed on file close.'):

		# Close file object.
		self.f.close()

		print "File closed."

		if commit:

			if self.mode is not 'r':

				if self.dirty:

					print "File dirty."

					self.commit(message)

					print "File committed."

	def _fread(self):

		return self.f.read

	read = property(_fread)

	def _fseek(self):

		return self.f.seek

	seek = property(_fseek)

	def _fwrite(self):

		return self.f.write

	write = property(_fwrite)
	
	def __del__(self):

		try:

			self.close(message='Changes committed on object destruction.')

		except:

			"""
			Potential exceptions upon closing the smartfile object include:
			"BadObject('HEAD')" if there is no commit history.
			"""

			self.f.close()
