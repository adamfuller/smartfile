** Aspirations **

To provide a file-like object (i.e one with `read`, `write`, and `close` methods) but which is actually a file in a `git` repository, the main feature being an automatic commit-upon-write behaviour which would provide sort of invisible undo history for text files (the latter part being the "smart" element).

** Reality **

This code isn't working yet and I haven't touched it in a few months.

** Cry for help **

If this sounds interesting to you, please do get in touch to help.  Or, if you know that it already exists, kindly point me in that direction.